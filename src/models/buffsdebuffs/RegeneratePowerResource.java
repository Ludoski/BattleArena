package models.buffsdebuffs;

public class RegeneratePowerResource extends BuffDebuff {

    public RegeneratePowerResource(){
        this.name = "Regenerate Power Resource";
        this.description = "Get a small amount of power resource.";
        this.duration = 1;
        this.statChange = "currentPowerResource";
        this.statChangeAmount = 3;
        this.target = "attacker";
    }
}
