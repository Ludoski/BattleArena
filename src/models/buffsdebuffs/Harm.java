package models.buffsdebuffs;

public class Harm extends BuffDebuff {

    public Harm(){
        this.name = "Harm";
        this.description = "Instantly lowers current health.";
        this.duration = 1;
        this.statChange = "currentHealth";
        this.statChangeAmount = -10;
        this.target = "defender";
    }
}
