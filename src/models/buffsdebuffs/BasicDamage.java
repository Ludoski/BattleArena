package models.buffsdebuffs;

public class BasicDamage extends BuffDebuff {

    public BasicDamage(){
        this.name = "Basic Damage";
        this.description = "Deal basic damage.";
        this.duration = 1;
        this.statChange = "currentHealth";
        this.statChangeAmount = -1;
        this.target = "defender";
    }
}
