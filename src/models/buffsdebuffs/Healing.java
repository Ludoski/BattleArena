package models.buffsdebuffs;

public class Healing extends BuffDebuff{

    public Healing(){
        this.name = "Healing";
        this.description = "Instantly raises current health.";
        this.duration = 1;
        this.statChange = "currentHealth";
        this.statChangeAmount = 10;
        this.target = "attacker";
    }
}
