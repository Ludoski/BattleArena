package models.buffsdebuffs;

public class SlowRegenarateHealth extends BuffDebuff{

    public SlowRegenarateHealth(){
        this.name = "Slow Regenerate Health";
        this.description = "Some health is gained.";
        this.duration = 1;
        this.statChange = "currentHealth";
        this.statChangeAmount = 1;
        this.target = "defender";
    }
}
