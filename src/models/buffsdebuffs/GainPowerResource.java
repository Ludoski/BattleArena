package models.buffsdebuffs;

public class GainPowerResource extends BuffDebuff {

    public GainPowerResource(){
        this.name = "Gain Power Resource";
        this.description = "Some power is gained.";
        this.duration = 1;
        this.statChange = "currentPowerResource";
        this.statChangeAmount = 1;
        this.target = "attacker";
    }
}
