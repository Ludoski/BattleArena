package models.buffsdebuffs;

public class TapPowerResource extends BuffDebuff{

    public TapPowerResource(){
        this.name = "Tap Power Resource";
        this.description = "Lot of power is gained.";
        this.duration = 1;
        this.statChange = "currentPowerResource";
        this.statChangeAmount = 4;
        this.target = "attacker";
    }
}
