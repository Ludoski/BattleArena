package models.buffsdebuffs;

public class RegenerateHealth extends BuffDebuff {

    public RegenerateHealth(){
        this.name = "Regenerate Health";
        this.description = "Slowly regenerates current health.";
        this.duration = 5;
        this.statChange = "currentHealth";
        this.statChangeAmount = 1;
        this.target = "attacker";
    }
}
