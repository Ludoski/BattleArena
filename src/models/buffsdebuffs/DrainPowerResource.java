package models.buffsdebuffs;

public class DrainPowerResource extends BuffDebuff {

    public DrainPowerResource(){
        this.name = "Drain Power Resource";
        this.description = "Lot of power is drained.";
        this.duration = 1;
        this.statChange = "currentPowerResource";
        this.statChangeAmount = -4;
        this.target = "defender";
    }
}
