package models.buffsdebuffs;

public class BuffDebuff {

    String name;
    String description;
    int duration;
    String statChange;
    int statChangeAmount;
    String target;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getStatChange() {
        return statChange;
    }

    public void setStatChange(String statChange) {
        this.statChange = statChange;
    }

    public int getStatChangeAmount() {
        return statChangeAmount;
    }

    public void setStatChangeAmount(int statChangeAmount) {
        this.statChangeAmount = statChangeAmount;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
