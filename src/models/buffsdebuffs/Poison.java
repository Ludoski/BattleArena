package models.buffsdebuffs;

public class Poison extends BuffDebuff {

    public Poison(){
        this.name = "Poison";
        this.description = "Deals damage over time.";
        this.duration = 3;
        this.statChange = "currentHealth";
        this.statChangeAmount = -1;
        this.target = "defender";
    }
}
