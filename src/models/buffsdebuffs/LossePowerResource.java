package models.buffsdebuffs;

public class LossePowerResource extends BuffDebuff {

    public LossePowerResource(){
        this.name = "Losse Power Resource";
        this.description = "Some power is lost.";
        this.duration = 1;
        this.statChange = "currentPowerResource";
        this.statChangeAmount = -1;
        this.target = "defender";
    }
}
