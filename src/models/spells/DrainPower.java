package models.spells;

import models.buffsdebuffs.DrainPowerResource;

public class DrainPower extends Spell {

    public DrainPower(){
        this.name = "Drain Power";
        this.spellDescription = "Drains lot of power resource.";
        this.buffsdebuffs.add(new DrainPowerResource());
        this.amountOfPowerResourceRequiredToCast = 4;
        this.cooldown = 3;
        this.currentCooldown = 3;
    }
}
