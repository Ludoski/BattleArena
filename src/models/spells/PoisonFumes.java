package models.spells;

import models.buffsdebuffs.Poison;

public class PoisonFumes extends Spell {

    public PoisonFumes(){
        this.name = "Poison Fumes";
        this.spellDescription = "Poisons opponent.";
        this.buffsdebuffs.add(new Poison());
        this.amountOfPowerResourceRequiredToCast = 6;
        this.cooldown = 3;
        this.currentCooldown = 3;
    }
}
