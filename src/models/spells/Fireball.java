package models.spells;

import models.buffsdebuffs.Harm;

public class Fireball extends Spell {

    public Fireball(){
        this.name = "Fireball";
        this.spellDescription = "Throws ball of fire.";
        this.buffsdebuffs.add(new Harm());
        this.amountOfPowerResourceRequiredToCast = 6;
        this.cooldown = 3;
        this.currentCooldown = 3;
    }
}
