package models.spells;

import models.buffsdebuffs.BasicDamage;
import models.buffsdebuffs.DrainPowerResource;

public class VampiricSlash extends Spell {

    public VampiricSlash(){
        this.name = "Vampiric Slash";
        this.spellDescription = "Harms and drains lot of power.";
        this.buffsdebuffs.add(new BasicDamage());
        this.buffsdebuffs.add(new DrainPowerResource());
        this.amountOfPowerResourceRequiredToCast = 8;
        this.cooldown = 4;
        this.currentCooldown = 4;
    }
}
