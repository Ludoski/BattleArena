package models.spells;

import models.buffsdebuffs.Harm;
import models.buffsdebuffs.Poison;

public class Smite extends Spell {

    public Smite(){
        this.name = "Smite";
        this.spellDescription = "God will smite them.";
        this.buffsdebuffs.add(new Harm());
        this.buffsdebuffs.add(new Poison());
        this.amountOfPowerResourceRequiredToCast = 10;
        this.cooldown = 3;
        this.currentCooldown = 3;
    }
}
