package models.spells;

import models.buffsdebuffs.Harm;

public class ShieldBash extends Spell {

    public ShieldBash(){
        this.name = "Shield Bash";
        this.spellDescription = "Shield bash.";
        this.buffsdebuffs.add(new Harm());
        this.amountOfPowerResourceRequiredToCast = 5;
        this.cooldown = 3;
        this.currentCooldown = 3;
    }
}
