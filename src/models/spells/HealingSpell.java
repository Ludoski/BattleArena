package models.spells;

import models.buffsdebuffs.Healing;

public class HealingSpell extends Spell{

    public HealingSpell(){
        this.name = "Healing Spell";
        this.spellDescription = "Healing spell.";
        this.buffsdebuffs.add(new Healing());
        this.amountOfPowerResourceRequiredToCast = 7;
        this.cooldown = 5;
        this.currentCooldown = 5;
    }
}
