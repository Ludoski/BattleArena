package models.spells;

import models.buffsdebuffs.BasicDamage;
import models.buffsdebuffs.RegenerateHealth;

public class Regenerate extends Spell{

    public Regenerate(){
        this.name = "Regenerate";
        this.spellDescription = "Stabs and regenerate health.";
        this.buffsdebuffs.add(new BasicDamage());
        this.buffsdebuffs.add(new RegenerateHealth());
        this.amountOfPowerResourceRequiredToCast = 8;
        this.cooldown = 6;
        this.currentCooldown = 6;
    }
}
