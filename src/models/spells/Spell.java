package models.spells;

import models.buffsdebuffs.BuffDebuff;

import java.util.ArrayList;

public class Spell {

    String name;
    String spellDescription;
    ArrayList<BuffDebuff> buffsdebuffs = new ArrayList<>();
    int amountOfPowerResourceRequiredToCast;
    int cooldown;
    int currentCooldown;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpellDescription() {
        return spellDescription;
    }

    public void setSpellDescription(String spellDescription) {
        this.spellDescription = spellDescription;
    }

    public ArrayList<BuffDebuff> getBuffsdebuffs() {
        return buffsdebuffs;
    }

    public void setBuffsdebuffs(ArrayList<BuffDebuff> buffsdebuffs) {
        this.buffsdebuffs = buffsdebuffs;
    }

    public int getAmountOfPowerResourceRequiredToCast() {
        return amountOfPowerResourceRequiredToCast;
    }

    public void setAmountOfPowerResourceRequiredToCast(int amountOfPowerResourceRequiredToCast) {
        this.amountOfPowerResourceRequiredToCast = amountOfPowerResourceRequiredToCast;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public int getCurrentCooldown() {
        return currentCooldown;
    }

    public void setCurrentCooldown(int currentCooldown) {
        this.currentCooldown = currentCooldown;
    }
}
