package models.spells;

import models.buffsdebuffs.BasicDamage;

public class BasicAttack extends Spell {

    public BasicAttack(){
        this.name = "Basic attack";
        this.spellDescription = "Deals basic damage.";
        this.buffsdebuffs.add(new BasicDamage());
        this.amountOfPowerResourceRequiredToCast = 0;
        this.cooldown = 1;
        this.currentCooldown = 1;
    }
}
