package models.spells;

import models.buffsdebuffs.TapPowerResource;

public class TapPower extends Spell {

    public TapPower(){
        this.name = "Tap Power";
        this.spellDescription = "Taps lot of power resource.";
        this.buffsdebuffs.add(new TapPowerResource());
        this.amountOfPowerResourceRequiredToCast = 4;
        this.cooldown = 3;
        this.currentCooldown = 3;
    }
}
