package models.arenas;

import models.buffsdebuffs.SlowRegenarateHealth;

public class RestorationArena extends Arena {

    public RestorationArena(){
        this.name = "Restoration Arena";
        this.buffDebuffs.add(new SlowRegenarateHealth());
        this.arenaDescription = "In this arena health is slowly regenerated.";
    }
}
