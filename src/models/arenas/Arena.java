package models.arenas;

import models.buffsdebuffs.BuffDebuff;

import java.util.ArrayList;

public class Arena {

    String name;
    ArrayList<BuffDebuff> buffDebuffs = new ArrayList<>();
    String arenaDescription;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<BuffDebuff> getBuffDebuffs() {
        return buffDebuffs;
    }

    public void setBuffDebuffs(ArrayList<BuffDebuff> buffDebuffs) {
        this.buffDebuffs = buffDebuffs;
    }

    public String getArenaDescription() {
        return arenaDescription;
    }

    public void setArenaDescription(String arenaDescription) {
        this.arenaDescription = arenaDescription;
    }
}
