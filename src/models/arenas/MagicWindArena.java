package models.arenas;

import models.buffsdebuffs.LossePowerResource;

public class MagicWindArena extends Arena {

    public MagicWindArena(){
        this.name = "Magic Wind Arena";
        this.buffDebuffs.add(new LossePowerResource());
        this.arenaDescription = "This arena have strong change of magic winds resulting in slow power flow.";
    }
}
