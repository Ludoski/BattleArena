package models.heroes;

import models.spells.*;

public class Mage extends Hero {

    public Mage(){
        this.heroName = "Mage";
        this.maxHealth = 60;
        this.currentHealth = maxHealth;
        this.maxPowerResource = 30;
        this.currentPowerResource = maxPowerResource;
        this.minBaseDamage = 2;
        this.maxBaseDamage = 4;
        this.criticalStrikeChance = 30;
        this.heroClass = "mage";
        this.classDescription = "Mage is weakest,\nwith high damage and \nabundant power.";
        this.spells.add(new BasicAttack());
        this.spells.add(new Fireball());
        this.spells.add(new DrainPower());
        this.spells.add(new TapPower());
    }
}
