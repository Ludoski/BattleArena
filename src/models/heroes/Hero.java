package models.heroes;

import models.buffsdebuffs.BuffDebuff;
import models.spells.Spell;

import java.util.ArrayList;

public class Hero {

    String heroName;
    int maxHealth;
    int currentHealth;
    ArrayList<BuffDebuff> buffsdebuffs = new ArrayList<>();
    int maxPowerResource;
    int currentPowerResource;
    int minBaseDamage;
    int maxBaseDamage;
    int criticalStrikeChance;
    static final int maxLevel = 20;
    int currentLevel;
    int maxExperience;
    int currentExperience;
    String heroClass;
    String classDescription;
    ArrayList<Spell> spells = new ArrayList<>();
    String currentSpell;
    int currentHealthThisTurn;
    int currentPowerResourceThisTurn;

    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public ArrayList<BuffDebuff> getBuffsdebuffs() {
        return buffsdebuffs;
    }

    public void setBuffsdebuffs(ArrayList<BuffDebuff> buffsdebuffs) {
        this.buffsdebuffs = buffsdebuffs;
    }

    public void addBuffsDebbufs(BuffDebuff buffDebuff){
        this.buffsdebuffs.add(buffDebuff);
    }

    public int getMaxPowerResource() {
        return maxPowerResource;
    }

    public void setMaxPowerResource(int maxPowerResource) {
        this.maxPowerResource = maxPowerResource;
    }

    public int getCurrentPowerResource() {
        return currentPowerResource;
    }

    public void setCurrentPowerResource(int currentPowerResource) {
        this.currentPowerResource = currentPowerResource;
    }

    public int getMinBaseDamage() {
        return minBaseDamage;
    }

    public void setMinBaseDamage(int minBaseDamage) {
        this.minBaseDamage = minBaseDamage;
    }

    public int getMaxBaseDamage() {
        return maxBaseDamage;
    }

    public void setMaxBaseDamage(int maxBaseDamage) {
        this.maxBaseDamage = maxBaseDamage;
    }

    public int getCriticalStrikeChance() {
        return criticalStrikeChance;
    }

    public void setCriticalStrikeChance(int criticalStrikeChance) {
        this.criticalStrikeChance = criticalStrikeChance;
    }

    public static int getMaxLevel() {
        return maxLevel;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int getMaxExperience() {
        maxExperience = currentLevel * 1000;
        return maxExperience;
    }

    public void setMaxExperience(int maxExperience) {
        this.maxExperience = maxExperience;
    }

    public int getCurrentExperience() {
        return currentExperience;
    }

    public void setCurrentExperience(int currentExperience) {
        this.currentExperience = currentExperience;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    public String getClassDescription() {
        return classDescription;
    }

    public void setClassDescription(String classDescription) {
        this.classDescription = classDescription;
    }

    public ArrayList<Spell> getSpells() {
        return spells;
    }

    public void setSpells(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    public String getCurrentSpell() {
        return currentSpell;
    }

    public void setCurrentSpell(String currentSpell) {
        this.currentSpell = currentSpell;
    }

    public int getCurrentHealthThisTurn() {
        return currentHealthThisTurn;
    }

    public void setCurrentHealthThisTurn(int currentHealthThisTurn) {
        this.currentHealthThisTurn = currentHealthThisTurn;
    }

    public int getCurrentPowerResourceThisTurn() {
        return currentPowerResourceThisTurn;
    }

    public void setCurrentPowerResourceThisTurn(int currentPowerResourceThisTurn) {
        this.currentPowerResourceThisTurn = currentPowerResourceThisTurn;
    }
}
