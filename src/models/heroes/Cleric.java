package models.heroes;

import models.spells.BasicAttack;
import models.spells.HealingSpell;
import models.spells.PoisonFumes;
import models.spells.Smite;

public class Cleric extends Hero {

    public Cleric(){
        this.heroName = "Cleric";
        this.maxHealth = 80;
        this.currentHealth = maxHealth;
        this.maxPowerResource = 20;
        this.currentPowerResource = maxPowerResource;
        this.minBaseDamage = 3;
        this.maxBaseDamage = 5;
        this.criticalStrikeChance = 40;
        this.heroClass = "cleric";
        this.classDescription = "Cleric is average strong,\nwith average damage\nand average power.";
        this.spells.add(new BasicAttack());
        this.spells.add(new HealingSpell());
        this.spells.add(new PoisonFumes());
        this.spells.add(new Smite());
    }
}
