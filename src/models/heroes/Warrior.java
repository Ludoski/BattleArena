package models.heroes;

import models.spells.BasicAttack;
import models.spells.Regenerate;
import models.spells.ShieldBash;
import models.spells.VampiricSlash;

public class Warrior extends Hero {

    public Warrior(){
        this.heroName = "Warrior";
        this.maxHealth = 100;
        this.currentHealth = maxHealth;
        this.maxPowerResource = 10;
        this.currentPowerResource = maxPowerResource;
        this.minBaseDamage = 4;
        this.maxBaseDamage = 6;
        this.criticalStrikeChance = 20;
        this.heroClass = "warrior";
        this.classDescription = "Warrior is strongest,\nwith moderate damage\nand low on power.";
        this.spells.add(new BasicAttack());
        this.spells.add(new ShieldBash());
        this.spells.add(new Regenerate());
        this.spells.add(new VampiricSlash());
    }
}
