package scenes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import services.ConnectionService;
import services.GUIService;

import java.util.Map;

public class StatisticsScene {

    public StatisticsScene(ConnectionService connectionService, GUIService guiService, Map<String, Integer> healthStatistics, Map<String, Integer> powerStatistics, Stage stage) {
        GridPane gpMain = new GridPane();
        gpMain.setAlignment(Pos.CENTER);
        gpMain.setId("gpMain");

        Label statisticsTitle = new Label("Statistics");
        statisticsTitle.setId("statisticsTitle");
        gpMain.setHalignment(statisticsTitle, HPos.CENTER);
        gpMain.add(statisticsTitle, 0, 0);

        HBox fpCharts = new HBox();
        ObservableList<PieChart.Data> pieChartHealthData = FXCollections.observableArrayList();
        healthStatistics.forEach((key, value) -> pieChartHealthData.add(new PieChart.Data((String)key, (int)value)));
        PieChart pieChartHealth = new PieChart(pieChartHealthData);
        pieChartHealth.setTitle("Health lost");
        fpCharts.getChildren().add(pieChartHealth);
        ObservableList<PieChart.Data> pieChartPowerData = FXCollections.observableArrayList();
        powerStatistics.forEach((key, value) -> pieChartPowerData.add(new PieChart.Data((String) key, (int) value)));
        PieChart pieChartPower = new PieChart(pieChartPowerData);
        pieChartPower.setTitle("Power used");
        fpCharts.getChildren().add(pieChartPower);
        gpMain.add(fpCharts, 0, 1);

        Button btnQuit = new Button("Quit");
        gpMain.setHalignment(btnQuit, HPos.CENTER);
        btnQuit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    new SetupScene(stage, connectionService, guiService);
            }
        });
        gpMain.add(btnQuit, 0, 2);

        //Set scene and show
        Scene scene = new Scene(gpMain, 1200, 700);
        scene.getStylesheets().addAll("style/General.css", "style/StatisticsScene.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
}
