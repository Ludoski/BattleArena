package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import services.ConnectionService;
import javafx.scene.Scene;
import javafx.stage.Stage;
import services.GUIServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginScene {

    private ConnectionService connectionService;
    private Stage stage;
    private String username;
    private TextField tfUsername, tfPassword;
    private Label lblStatusLine;

    public LoginScene(Stage stage, ConnectionService connectionService) {
        this.stage = stage;
        this.connectionService = connectionService;
        createScene();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private void createScene(){
        //Create main grid pane
        GridPane gpMain = new GridPane();
        gpMain.setAlignment(Pos.CENTER);
        gpMain.setId("gpMain");

        //login scene info
        VBox vbInfo = new VBox();
        vbInfo.setAlignment(Pos.CENTER);
        Text loginTitle = new Text();
        loginTitle.setText("Battle Arena");
        loginTitle.setId("loginTitle");
        vbInfo.getChildren().add(loginTitle);
        Label lblLoginDescription = new Label("Login to your account or register a new one.");
        vbInfo.getChildren().add(lblLoginDescription);
        gpMain.add(vbInfo,0 ,0);

        //Text field area
        GridPane gpTextFields = new GridPane();
        gpTextFields.setId("gpTextFields");
        Label lblUsernameLabel = new Label("Username:");
        gpTextFields.add(lblUsernameLabel, 0, 0);
        Label lblPasswordLabel = new Label("Password:");
        gpTextFields.add(lblPasswordLabel, 0, 1);
        tfUsername = new TextField();
        gpTextFields.add(tfUsername, 1, 0);
        tfPassword = new TextField();
        gpTextFields.add(tfPassword, 1, 1);
        gpMain.add(gpTextFields, 0, 1);

        //Button area
        HBox hbButtons = new HBox();
        hbButtons.setAlignment(Pos.CENTER);
        Button btnLogin = new Button("Login");
        btnLogin.defaultButtonProperty().bind(btnLogin.focusedProperty());
        btnLogin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Prevents empty TextFields
                if(!((tfUsername.getText().equals("") && (tfPassword.getText().equals(""))))){
                    login();
                } else{
                    lblStatusLine.setText("Username or password can't be empty!");
                }
            }
        });
        hbButtons.getChildren().add(btnLogin);
        Button btnRegister = new Button("Register");
        btnRegister.defaultButtonProperty().bind(btnRegister.focusedProperty());
        btnRegister.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Prevents empty TextFields
                if(!((tfUsername.getText().equals("") && (tfPassword.getText().equals(""))))){
                    register();
                } else{
                    lblStatusLine.setText("Username or password can't be empty!");
                }
            }
        });
        hbButtons.getChildren().add(btnRegister);
        gpMain.add(hbButtons, 0, 2);

        //Create status line
        lblStatusLine = new Label("Status line.");
        gpMain.add(lblStatusLine, 0, 4);

        //Set scene and show
        Scene scene = new Scene(gpMain, 1200, 700);
        scene.getStylesheets().addAll("style/General.css", "style/LoginScene.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    //When SignIn button is pressed checks username and password and loads Setup scene
    public void login(){
        ResultSet result = connectionService.readUser(tfUsername.getText(), tfPassword.getText());
        try {
            if(result.next()){
                username = tfUsername.getText();
                connectionService.setId(result.getInt("id"));
                connectionService.setUsername(username);
                loadSetup();
            } else{
                lblStatusLine.setText("User does not exist\nor you entered wrong password.");
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    //When SignUp button is pressed checks username and password
    // and if not exists creates user, heroes and loads Setup scene
    public void register(){
        ResultSet result = connectionService.readUser(tfUsername.getText(), tfPassword.getText());
        try{
            if(!result.next()){
                connectionService.createUser(tfUsername.getText(), tfPassword.getText());
                username = tfUsername.getText();
                connectionService.createHero("warrior");
                connectionService.createHero("mage");
                connectionService.createHero("cleric");
                result = connectionService.readUser(username);
                try {
                    if(result.next()){
                        connectionService.setUsername(result.getString("username"));
                    }
                } catch(SQLException e){
                    e.printStackTrace();
                }
                loadSetup();
            } else{
                lblStatusLine.setText("User already exists.");
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    //Loads Setup scene
    private void loadSetup(){
        new SetupScene(stage, connectionService, new GUIServiceImpl());
    }


}
