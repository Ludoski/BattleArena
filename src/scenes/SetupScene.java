package scenes;

import battlemodes.HeroVsAI;
import battlemodes.TwoVsTwo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import models.arenas.Arena;
import models.arenas.MagicWindArena;
import models.arenas.RestorationArena;
import models.heroes.Hero;
import services.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SetupScene {

    private ConnectionService connectionService;
    private BattleModeService battleModeService;
    private LoggingService loggingService;
    private GUIService guiService;
    private VBox vbHeroStats;
    private Stage stage;
    private Hero hero;
    private Arena arena;
    private String battleMode;
    private int selectedHero;

    public SetupScene(Stage stage, ConnectionService connectionService, GUIService guiService) {
        this.stage = stage;
        this.connectionService = connectionService;
        this.loggingService = new LoggingServiceImpl();
        this.battleModeService = new BattleModeServiceImpl(loggingService, connectionService);
        this.guiService = guiService;
        createScene();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void createScene(){
        //Create main grid pane
        GridPane gpMain = new GridPane();
        gpMain.setAlignment(Pos.CENTER);
        gpMain.setId("gpMain");

        //Setup scene info
        VBox vbInfo = new VBox();
        vbInfo.setAlignment(Pos.CENTER);
        Text setupTitle = new Text();
        setupTitle.setText("Setup your battle " + connectionService.getUsername() + ".");
        setupTitle.setId("setupTitle");
        vbInfo.getChildren().add(setupTitle);
        Label lblSetupDescription = new Label("Select what kind of battle you want to play.");
        vbInfo.getChildren().add(lblSetupDescription);
        gpMain.add(vbInfo,0 ,0,GridPane.REMAINING, 1);

        //Hero stats
        GridPane gpHero = new GridPane();
        gpHero.setId("gpHero");
        VBox vbHeroStatsLabels = new VBox();
        vbHeroStatsLabels = guiService.createAllHeroLabels(vbHeroStatsLabels);
        gpHero.add(vbHeroStatsLabels, 0, 0);
        vbHeroStats = new VBox();
        vbHeroStats = guiService.createAllHeroLabels(vbHeroStats);
        gpHero.add(vbHeroStats, 1, 0);
        gpMain.add(gpHero, 1, 1);

        //VBox for all choiceboxes
        VBox vbChoices = new VBox();
        vbChoices.setId("vbChoices");

        //Select arena
        VBox vbArena = new VBox();
        vbArena.setAlignment(Pos.CENTER);
        Label lblArena = new Label("Select arena:");
        vbArena.getChildren().add(lblArena);
        ChoiceBox cbArena = new ChoiceBox();
        cbArena.setItems(FXCollections.observableArrayList(
                "Magic Wind Arena",
                "Regeneration Arena"
        ));
        Label lblArenaDescription = new Label();
        lblArenaDescription.setId("lblArenaDescription");
        cbArena.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                switch((int) newValue){
                    case 0:{
                        arena = new MagicWindArena();
                        break;
                    }
                    case 1:{
                        arena = new RestorationArena();
                        break;
                    }
                }
                lblArenaDescription .setText(arena.getArenaDescription());
            }
        });
        cbArena.getSelectionModel().selectFirst();
        vbArena.getChildren().add(cbArena);
        vbArena.getChildren().add(lblArenaDescription);
        vbChoices.getChildren().add(vbArena);

        //Select hero
        VBox vbHero = new VBox();
        vbHero.setAlignment(Pos.CENTER);
        Label lblHero = new Label("Select hero:");
        vbHero.getChildren().add(lblHero);
        ChoiceBox cbHero = new ChoiceBox();
        cbHero.setItems(FXCollections.observableArrayList(
                "Warrior",
                "Mage",
                "Cleric"
        ));
        cbHero.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                selectedHero = (int) newValue;
                refreshCbHero(selectedHero);
            }
        });
        cbHero.getSelectionModel().selectFirst();
        vbHero.getChildren().add(cbHero);
        vbChoices.getChildren().add(vbHero);

        //Select battle mode
        VBox vbBattleMode = new VBox();
        vbBattleMode.setAlignment(Pos.CENTER);
        Label lblBattleMode = new Label("Select battle mode:");
        vbBattleMode.getChildren().add(lblBattleMode);
        ChoiceBox cbBattleMode = new ChoiceBox();
        cbBattleMode.setItems(FXCollections.observableArrayList(
                "Hero vs AI",
                "Two vs two"
        ));
        cbBattleMode.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                switch((int) newValue){
                    case 0:{
                        battleMode = "HeroVsAI";
                        break;
                    }
                    case 1:{
                        battleMode = "TwoVsTwo";
                        break;
                    }
                }
            }
        });
        cbBattleMode.getSelectionModel().selectFirst();
        vbBattleMode.getChildren().add(cbBattleMode);
        vbChoices.getChildren().add(vbBattleMode);
        gpMain.add(vbChoices, 0, 1);

        //Button Start
        Button btnStart = new Button("Start");
        btnStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                startBattle();
            }
        });
        gpMain.add(btnStart, 1, 2);

        //Vbox for renaming hero
        VBox vbRename = new VBox();
        vbRename.setId("vbRename");
        Label lblRename = new Label("Enter new hero name: ");
        vbRename.getChildren().add(lblRename);
        TextField tfRename = new TextField();
        vbRename.getChildren().add(tfRename);
        Button btnRename = new Button("Rename hero");
        btnRename.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hero.setHeroName(tfRename.getText());
                connectionService.updateHero(hero);
                refreshCbHero(selectedHero);
            }
        });
        vbRename.getChildren().add(btnRename);
        gpMain.add(vbRename, 2, 1);

        //Create HBox for Delete hero and Delete user account buttons
        HBox hbDelete = new HBox();
        Button btnDeleteHero = new Button("Delete hero");
        btnDeleteHero.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                connectionService.deleteHero(hero);
                refreshCbHero(selectedHero);
            }
        });
        hbDelete.getChildren().add(btnDeleteHero);
        Button btnDeleteAccount = new Button("Delete account");
        btnDeleteAccount.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                connectionService.deleteUser();
                new LoginScene(stage, connectionService);
            }
        });
        hbDelete.getChildren().addAll(btnDeleteAccount);
        gpMain.add(hbDelete, 2, 2);

        //Set scene and show
        Scene scene = new Scene(gpMain, 1200, 700);
        scene.getStylesheets().addAll("style/General.css", "style/SetupScene.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    private void refreshCbHero(int newValue){
        ResultSet result = null;
        try{
            switch (newValue){
                case 0:{
                    hero = battleModeService.createNewHero("warrior");
                    break;
                }
                case 1:{
                    hero = battleModeService.createNewHero("mage");
                    break;
                }
                case 2:{
                    hero = battleModeService.createNewHero("cleric");
                    break;
                }
            }
            result = connectionService.readHero(hero.getHeroClass());
            result.next();
            hero.setHeroName(result.getString("name"));
            hero.setCurrentLevel(result.getInt("level"));
            hero.setCurrentExperience(result.getInt("experience"));
            guiService.displayAllHeroStats(vbHeroStats, hero);
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    //When Start button is pressed loads Arena scene
    public void startBattle(){
        switch(battleMode){
            case"HeroVsAI":{
                new HeroVsAI(loggingService, connectionService, guiService, battleModeService, hero, arena, stage);
                break;
            }
            case "TwoVsTwo":{
                new TwoVsTwo(loggingService, connectionService, guiService, battleModeService, hero, arena, stage);
                break;
            }
        }
    }
}
