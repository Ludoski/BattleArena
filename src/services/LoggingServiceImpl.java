package services;


import models.heroes.Hero;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggingServiceImpl implements LoggingService {

    private static Logger logger;

    public LoggingServiceImpl(){
        logger = LogManager.getLogger(LoggingServiceImpl.class);
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        LoggingServiceImpl.logger = logger;
    }

    public String getTurnInfo(int currentHealthThisTurn, int currentPowerResourceThisTurn, Hero combatant){
        String wordHealth = (currentHealthThisTurn < combatant.getCurrentHealth()) ? " gains " : " losses ";
        String wordPowerResource = (currentPowerResourceThisTurn < combatant.getCurrentPowerResource()) ? " gains " : " losses ";
        return (combatant.getHeroName() + wordHealth +  Math.abs(combatant.getCurrentHealthThisTurn() - combatant.getCurrentHealth()) + " health and"
                + wordPowerResource + Math.abs(combatant.getCurrentPowerResourceThisTurn() - combatant.getCurrentPowerResource()) + " power.");
    }
}
