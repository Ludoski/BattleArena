package services;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import models.buffsdebuffs.BuffDebuff;
import models.heroes.Hero;

public class GUIServiceImpl implements GUIService {

    public VBox createAllHeroLabels(VBox vbHeroLabels){
        vbHeroLabels = new VBox();
        for(int i = 0; i < 15; i++){
            Label label = new Label();
            vbHeroLabels.getChildren().add(label);
        }
        Label label0 = (Label) vbHeroLabels.getChildren().get(0);
        label0.setText("Name:");
        Label label1 = (Label) vbHeroLabels.getChildren().get(1);
        label1.setText("Max health:");
        Label label2 = (Label) vbHeroLabels.getChildren().get(2);
        label2.setText("Current health:");
        Label label3 = (Label) vbHeroLabels.getChildren().get(3);
        label3.setText("Buffs/debuffs:");
        Label label4 = (Label) vbHeroLabels.getChildren().get(4);
        label4.setText("Max power resource:");
        Label label5 = (Label) vbHeroLabels.getChildren().get(5);
        label5.setText("Current power resource:");
        Label label6 = (Label) vbHeroLabels.getChildren().get(6);
        label6.setText("Min base damage:");
        Label label7 = (Label) vbHeroLabels.getChildren().get(7);
        label7.setText("Max base damage:");
        Label label8 = (Label) vbHeroLabels.getChildren().get(8);
        label8.setText("Critical strike chance:");
        Label label9 = (Label) vbHeroLabels.getChildren().get(9);
        label9.setText("Max level:");
        Label label10 = (Label) vbHeroLabels.getChildren().get(10);
        label10.setText("Current level: ");
        Label label11 = (Label) vbHeroLabels.getChildren().get(11);
        label11.setText("Max experience:");
        Label label12 = (Label) vbHeroLabels.getChildren().get(12);
        label12.setText("Current experience:");
        Label label13 = (Label) vbHeroLabels.getChildren().get(13);
        label13.setText("Class:");
        Label label14 = (Label) vbHeroLabels.getChildren().get(14);
        label14.setWrapText(true);
        label14.setText("Class description:");
        return vbHeroLabels;
    }

    public VBox createBasicHeroLabels(VBox vbHeroLabels){
        vbHeroLabels = new VBox();
        for(int i = 0; i < 7; i++){
            Label label = new Label();
            vbHeroLabels.getChildren().add(label);
        }
        Label label0 = (Label) vbHeroLabels.getChildren().get(0);
        label0.setText("Name:");
        Label label1 = (Label) vbHeroLabels.getChildren().get(1);
        label1.setText("Max health:");
        Label label2 = (Label) vbHeroLabels.getChildren().get(2);
        label2.setText("Current health:");
        Label label3 = (Label) vbHeroLabels.getChildren().get(3);
        label3.setText("Buffs/debuffs:");
        Label label4 = (Label) vbHeroLabels.getChildren().get(4);
        label4.setText("Max power resource:");
        Label label5 = (Label) vbHeroLabels.getChildren().get(5);
        label5.setText("Current power resource:");
        Label label6 = (Label) vbHeroLabels.getChildren().get(6);
        label6.setText("Class:");
        return vbHeroLabels;
    }

    public VBox displayAllHeroStats(VBox vbHeroStats, Hero combatant){
        Label label0 = (Label) vbHeroStats.getChildren().get(0);
        label0.setText(combatant.getHeroName());
        Label label1 = (Label) vbHeroStats.getChildren().get(1);
        label1.setText("" + combatant.getMaxHealth());
        Label label2 = (Label) vbHeroStats.getChildren().get(2);
        label2.setText("" + combatant.getCurrentHealth());
        Label label3 = (Label) vbHeroStats.getChildren().get(3);
        label3.setText("");
        for(BuffDebuff bd : combatant.getBuffsdebuffs()){
            label3.setText(label3.getText() + "*" + bd.getName() + "*");
        }
        Label label4 = (Label) vbHeroStats.getChildren().get(4);
        label4.setText("" + combatant.getMaxPowerResource());
        Label label5 = (Label) vbHeroStats.getChildren().get(5);
        label5.setText("" + combatant.getCurrentPowerResource());
        Label label6 = (Label) vbHeroStats.getChildren().get(6);
        label6.setText("" + combatant.getMinBaseDamage());
        Label label7 = (Label) vbHeroStats.getChildren().get(7);
        label7.setText("" + combatant.getMaxBaseDamage());
        Label label8 = (Label) vbHeroStats.getChildren().get(8);
        label8.setText("" + combatant.getCriticalStrikeChance() + "%");
        Label label9 = (Label) vbHeroStats.getChildren().get(9);
        label9.setText("" + combatant.getMaxLevel());
        Label label10 = (Label) vbHeroStats.getChildren().get(10);
        label10.setText("" + combatant.getCurrentLevel());
        Label label11 = (Label) vbHeroStats.getChildren().get(11);
        label11.setText("" + combatant.getMaxExperience());
        Label label12 = (Label) vbHeroStats.getChildren().get(12);
        label12.setText("" + combatant.getCurrentExperience());
        Label label13 = (Label) vbHeroStats.getChildren().get(13);
        label13.setText(combatant.getHeroClass());
        Label label14 = (Label) vbHeroStats.getChildren().get(14);
        label14.setText(combatant.getClassDescription());
        return vbHeroStats;
    }

    public VBox displayBasicHeroStats(VBox vbHeroStats, Hero combatant){
        Label label0 = (Label) vbHeroStats.getChildren().get(0);
        label0.setText(combatant.getHeroName());
        Label label1 = (Label) vbHeroStats.getChildren().get(1);
        label1.setText("" + combatant.getMaxHealth());
        Label label2 = (Label) vbHeroStats.getChildren().get(2);
        label2.setText("" + combatant.getCurrentHealth());
        Label label3 = (Label) vbHeroStats.getChildren().get(3);
        label3.setText("");
        for(BuffDebuff bd : combatant.getBuffsdebuffs()){
            label3.setText(label3.getText() + "*" + bd.getName() + "*");
        }
        Label label4 = (Label) vbHeroStats.getChildren().get(4);
        label4.setText("" + combatant.getMaxPowerResource());
        Label label5 = (Label) vbHeroStats.getChildren().get(5);
        label5.setText("" + combatant.getCurrentPowerResource());
        Label label6 = (Label) vbHeroStats.getChildren().get(6);
        label6.setText(combatant.getHeroClass());
        return vbHeroStats;
    }
}
