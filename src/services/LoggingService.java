package services;

import models.heroes.Hero;
import org.apache.logging.log4j.Logger;

public interface LoggingService {

    Logger getLogger();
    void setLogger(Logger logger);
    String getTurnInfo(int currentHealthThisTurn, int currentPowerResourceThisTurn, Hero combatant);
}
