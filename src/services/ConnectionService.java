package services;

import models.heroes.Hero;

import java.sql.ResultSet;

public interface ConnectionService {

    int getId();
    void setId(int id);
    String getUsername();
    void setUsername(String username);
    void connectToSqlite();
    void createTables();
    void createUser(String username, String password);
    ResultSet readUser(String username);
    ResultSet readUser(String username, String password);
    void updateUser();
    void deleteUser();
    void createHero(String heroClass);
    ResultSet readHero(String heroClass);
    void updateHero(Hero hero);
    void deleteHero(Hero hero);
}
