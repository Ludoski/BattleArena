package services;

import models.heroes.Hero;

import java.sql.*;

public class ConnectionServiceSqliteImpl implements ConnectionService{

    private static Connection connection;
    private static int id;
    private static String username;

    public ConnectionServiceSqliteImpl(){
        connectToSqlite();
        //Create tables in database if not exists
        createTables();
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void connectToSqlite(){
        try{
            String url = "jdbc:sqlite:sqlite/battle_arena.sqlite";
            connection = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
        } catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void createTables(){
        try{
            String sql = "CREATE TABLE IF NOT EXISTS users(" +
                            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "username VARCHAR(128)," +
                            "password VARCHAR(128))";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.execute();
        } catch(SQLException e){
            e.printStackTrace();
        }
        try{
            String sql = "CREATE TABLE IF NOT EXISTS heroes(" +
                            "id INTEGER," +
                            "heroclass VARCHAR(128)," +
                            "name VARCHAR(128)," +
                            "level INTEGER," +
                            "experience INTEGER)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.execute();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void createUser(String username, String password){
        try{
            String sql = "INSERT INTO users(username, password) VALUES(?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.execute();
        } catch (SQLException e){
            e.printStackTrace();
        }
        try{
            String sql = "SELECT * FROM users WHERE username = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            id = result.getInt("id");
            username = result.getString("username");
            result.close();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public ResultSet readUser(String username){
        ResultSet result = null;
        try{
            String sql = "SELECT * FROM users WHERE username = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            result = preparedStatement.executeQuery();
        } catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    public ResultSet readUser(String username, String password){
        ResultSet result = null;
        try{
            String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            result = preparedStatement.executeQuery();
        } catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    public void updateUser(){

    }

    public void deleteUser(){
        try{
            String sql = "DELETE FROM heroes WHERE id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            sql = "DELETE FROM users WHERE id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch(SQLException e){
            e.printStackTrace();
        }

    }

    public void createHero(String heroClass){
        try{
            String sql = "INSERT INTO heroes(id, heroclass, name, level, experience) VALUES(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, heroClass);
            preparedStatement.setString(3, heroClass);
            preparedStatement.setInt(4, 1);
            preparedStatement.setInt(5, 0);
            preparedStatement.execute();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public ResultSet readHero(String heroClass){
        ResultSet result = null;
        try{
            String sql = "SELECT * FROM heroes WHERE id = ? AND heroclass = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, heroClass);
            result = preparedStatement.executeQuery();
        } catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    public void updateHero(Hero hero){
        try {
            String sql = "UPDATE heroes SET name=?, level=?, experience=? WHERE id=? AND heroclass=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, hero.getHeroName());
            preparedStatement.setInt(2, hero.getCurrentLevel());
            preparedStatement.setInt(3, hero.getCurrentExperience());
            preparedStatement.setInt(4, id);
            preparedStatement.setString(5, hero.getHeroClass());
            preparedStatement.execute();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void deleteHero(Hero hero){
        try {
            String sql = "UPDATE heroes SET name=?, level=?, experience=? WHERE id=? AND heroclass=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, hero.getHeroClass());
            preparedStatement.setInt(2, 1);
            preparedStatement.setInt(3, 0);
            preparedStatement.setInt(4, id);
            preparedStatement.setString(5, hero.getHeroClass());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
