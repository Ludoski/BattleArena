package services;

import javafx.scene.layout.VBox;
import models.arenas.Arena;
import models.buffsdebuffs.BuffDebuff;
import models.heroes.Hero;
import models.spells.Spell;

import java.util.ArrayList;

public interface BattleModeService {

    BuffDebuff cloneBuffDebuff(BuffDebuff buffDebuff);
    BattleModeServiceImpl.RandomHeroName getRandomHeroName();
    Hero createNewHero(String heroClass);
    void addCombatantSpellBuffsDebuffs(Hero attacker, Hero defender);
    Spell getCombatantSpell(Hero combatant);
    int getHeroCriticalModifier(Hero combatant);
    int getDamageModifier(Hero combatant);
    void resolveArenaAndBattleModeBuffsDebuffs(Arena arena, BuffDebuff battleModeBuffsDebuffs, Hero combatant);
    void resolveCombatantBuffsDebuffs(Hero attacker);
    void removeFinishedBuffsDebuffs(Hero combatant);
    void resolveAllSpellCooldowns(Hero combatant);
    void refreshSpellsText(VBox combatantSpells, Hero combatant);
    void fixCurrentPowerResourceOverflow(Hero combatant);
    void fixCurrentHealthOverflow(Hero combatant);
    VBox setCombatantSpells(VBox vbCombatantSpells, Hero combatant);
    void setAISpell(Hero combatant);
    Hero selectOpponent(ArrayList<Hero> opponents);
    void calculateExperienceAndSaveHero(Hero hero, String victoryCondition);
}
