package services;

import javafx.scene.layout.VBox;
import models.heroes.Hero;

public interface GUIService {

    VBox createAllHeroLabels(VBox vbHeroLabels);
    VBox createBasicHeroLabels(VBox vbHeroLabels);
    VBox displayAllHeroStats(VBox vbHeroStats, Hero combatant);
    VBox displayBasicHeroStats(VBox vbHeroStats, Hero combatant);
}
