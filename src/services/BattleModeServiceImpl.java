package services;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import models.arenas.Arena;
import models.buffsdebuffs.BuffDebuff;
import models.heroes.Cleric;
import models.heroes.Hero;
import models.heroes.Mage;
import models.heroes.Warrior;
import models.spells.Spell;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class BattleModeServiceImpl implements BattleModeService{

    LoggingService loggingService;
    Logger logger;
    ConnectionService connectionService;
    public enum RandomHeroName{
        Theo, Garix, Marx, Laniel, Fadil, Lox, Manty, Claude, Popax, Hernes, Dante, Plox, Joyce, Brugia,
        Laura, Lenore, Cricket, Pax, Helena, Mia, Thea, Adria, Penelopa, Lora, Faust, Precle
    }

    public BattleModeServiceImpl(LoggingService loggingService, ConnectionService connectionService){
        this.loggingService = loggingService;
        this.logger = loggingService.getLogger();
        this.connectionService = connectionService;
    }

    public BuffDebuff cloneBuffDebuff(BuffDebuff buffDebuff){
        BuffDebuff clonedBuffDebuff = new BuffDebuff();
        clonedBuffDebuff.setName(buffDebuff.getName());
        clonedBuffDebuff.setDescription(buffDebuff.getDescription());
        clonedBuffDebuff.setDuration(buffDebuff.getDuration());
        clonedBuffDebuff.setStatChange(buffDebuff.getStatChange());
        clonedBuffDebuff.setStatChangeAmount(buffDebuff.getStatChangeAmount());
        clonedBuffDebuff.setTarget(buffDebuff.getTarget());
        return clonedBuffDebuff;
    }

    public RandomHeroName getRandomHeroName(){
        Random random = new Random();
        return RandomHeroName.values()[random.nextInt(RandomHeroName.values().length)];
    }

    public Hero createNewHero(String heroClass){
        if(heroClass.equals("random")){
            Random random = new Random();
            int heroClassIndex = random.nextInt(3);
            switch (heroClassIndex){
                case 0:{
                    heroClass = "warrior";
                    break;
                }
                case 1:{
                    heroClass = "mage";
                    break;
                }
                case 2:{
                    heroClass = "cleric";
                    break;
                }
            }
        }
        Hero newHero = null;
        switch(heroClass){
            case "warrior":{
                newHero = new Warrior();
                break;
            }
            case "mage":{
                newHero = new Mage();
                break;
            }
            case "cleric":{
                newHero = new Cleric();
                break;
            }
        }
        newHero.setHeroName(getRandomHeroName().toString());
        return newHero;
    }

    public void addCombatantSpellBuffsDebuffs(Hero attacker, Hero defender){
        if(attacker.getCurrentHealth() <= 0){
            return;
        }
        Spell spell = getCombatantSpell(attacker);
        attacker.setCurrentPowerResource(attacker.getCurrentPowerResource() - spell.getAmountOfPowerResourceRequiredToCast());
        spell.setCurrentCooldown(spell.getCurrentCooldown() - 1);
        if(spell.getCurrentCooldown() == 0){
            spell.setCurrentCooldown(spell.getCooldown());
        }
        for(BuffDebuff buffDebuff : spell.getBuffsdebuffs()){
            BuffDebuff clonedBuffDebuff = cloneBuffDebuff(buffDebuff);
            int attackerDamageModifier = getDamageModifier(attacker);
            if(clonedBuffDebuff.getStatChangeAmount() < 0){
                attackerDamageModifier = -attackerDamageModifier;
            }
            clonedBuffDebuff.setStatChangeAmount(clonedBuffDebuff.getStatChangeAmount() + attackerDamageModifier);
            if(clonedBuffDebuff.getTarget().equals("attacker")){
                attacker.addBuffsDebbufs(clonedBuffDebuff);
            }
            if(clonedBuffDebuff.getTarget().equals("defender")){
                defender.addBuffsDebbufs(clonedBuffDebuff);
            }
        }
        logger.info(attacker.getHeroName() + " uses " + attacker.getCurrentSpell() + ".");
    }

    public Spell getCombatantSpell(Hero combatant){
        for(Spell spell : combatant.getSpells()){
            if(combatant.getCurrentSpell().equals(spell.getName())){
                return spell;
            }
        }
        return null;
    }

    public int getHeroCriticalModifier(Hero combatant){
        int criticalStrikeModifier = 1;
        Random random = new Random();
        int critical = random.nextInt(100) + 1;
        if(critical < combatant.getCriticalStrikeChance()){
            criticalStrikeModifier = 2;
        }
        return criticalStrikeModifier;
    }

    public int getDamageModifier(Hero combatant){
        Random random = new Random();
        int criticalStrikeModifier = getHeroCriticalModifier(combatant);
        return (random.nextInt(combatant.getMaxBaseDamage() -  combatant.getMinBaseDamage() + 1) + combatant.getMinBaseDamage()) * criticalStrikeModifier;
    }

    private void changeHeroStats(Hero combatant, BuffDebuff buffDebuff) {
        switch(buffDebuff.getStatChange()){
            case "currentHealth":{
                int resolvedHealth = combatant.getCurrentHealth() + buffDebuff.getStatChangeAmount();
                String word = (combatant.getCurrentHealth() < resolvedHealth) ? " is healed for " : " is harmed for ";
                logger.info(combatant.getHeroName() + word + Math.abs(combatant.getCurrentHealth() - resolvedHealth) + " health from " + buffDebuff.getName() + ".");
                combatant.setCurrentHealth(resolvedHealth);
                break;
            }
            case "currentPowerResource":{
                int resolvedPowerResource = combatant.getCurrentPowerResource() + buffDebuff.getStatChangeAmount();
                String word = (combatant.getCurrentPowerResource() < resolvedPowerResource) ? " gained " : " losses ";
                logger.info(combatant.getHeroName() + word + Math.abs(combatant.getCurrentPowerResource() - resolvedPowerResource) + " power from " + buffDebuff.getName() + ".");
                combatant.setCurrentPowerResource(combatant.getCurrentPowerResource() + buffDebuff.getStatChangeAmount());
                break;
            }
        }
        buffDebuff.setDuration(buffDebuff.getDuration() - 1);
    }


    public void resolveArenaAndBattleModeBuffsDebuffs(Arena arena, BuffDebuff battleModeBuffsDebuffs, Hero combatant){
        if(combatant.getCurrentHealth() <= 0){
            return;
        }
        for(BuffDebuff buffDebuff : arena.getBuffDebuffs()){
            changeHeroStats(combatant, buffDebuff);
        }
        changeHeroStats(combatant, battleModeBuffsDebuffs);
    }

    public void resolveCombatantBuffsDebuffs(Hero combatant){
        for(BuffDebuff buffDebuff : combatant.getBuffsdebuffs()){
            changeHeroStats(combatant, buffDebuff);
        }
    }

    public void removeFinishedBuffsDebuffs(Hero combatant){
        for(Iterator<BuffDebuff> iterator = combatant.getBuffsdebuffs().iterator(); iterator.hasNext();){
            BuffDebuff bd = iterator.next();
            if(bd.getDuration() == 0) {
                iterator.remove();
            }
        }
    }

    public void resolveAllSpellCooldowns(Hero combatant){
        for(Spell spell : combatant.getSpells()) {
            if (spell.getCurrentCooldown() == 0) {
                spell.setCurrentCooldown(spell.getCooldown());
            }
            if(spell.getCurrentCooldown() < spell.getCooldown()) {
                spell.setCurrentCooldown(spell.getCurrentCooldown() - 1);
            }
        }
    }

    public void refreshSpellsText(VBox combatantSpells, Hero combatant){
        for(int i = 1; i<combatant.getSpells().size(); i++){
            RadioButton radioButton = new RadioButton();
            if(combatant.getSpells().get(i).getCurrentCooldown() < combatant.getSpells().get(i).getCooldown()){
                radioButton = (RadioButton) combatantSpells.getChildren().get(i);
                radioButton.setText(
                        combatant.getSpells().get(i).getName() +
                                " (cost: " + (combatant.getSpells().get(i).getAmountOfPowerResourceRequiredToCast()) + ")" +
                                " (cooldown: " + (combatant.getSpells().get(i).getCurrentCooldown() + 1) + ")");
            } else{
                radioButton = (RadioButton) combatantSpells.getChildren().get(i);
                radioButton.setText(
                        combatant.getSpells().get(i).getName() +
                                " (cost: " + (combatant.getSpells().get(i).getAmountOfPowerResourceRequiredToCast()) + ")");
            }
        }
    }

    public void fixCurrentPowerResourceOverflow(Hero combatant){
        if(combatant.getCurrentPowerResource() > combatant.getMaxPowerResource()){
            combatant.setCurrentPowerResource(combatant.getMaxPowerResource());
        }
        if(combatant.getCurrentPowerResource() < 0){
            combatant.setCurrentPowerResource(0);
        }
    }

    public void fixCurrentHealthOverflow(Hero combatant){
        if(combatant.getCurrentHealth() > combatant.getMaxHealth()){
            combatant.setCurrentHealth(combatant.getMaxHealth());
        }
        if(combatant.getCurrentHealth() < 0){
            combatant.setCurrentHealth(0);
        }
    }

    public VBox setCombatantSpells(VBox vbCombatantSpells, Hero combatant){
        ToggleGroup rbgCombatantRBGroup = new ToggleGroup();
        ArrayList<RadioButton> combatantSpellRadioButtons = new ArrayList<>();
        for(Spell spell : combatant.getSpells()){
            RadioButton rb = new RadioButton(
                    spell.getName() +
                            " (cost: " + (spell.getAmountOfPowerResourceRequiredToCast()) + ")");
            rb.setToggleGroup(rbgCombatantRBGroup);
            vbCombatantSpells.getChildren().add(rb);
            combatantSpellRadioButtons.add(rb);
        }
        rbgCombatantRBGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                int index = rbgCombatantRBGroup.getToggles().indexOf(rbgCombatantRBGroup.getSelectedToggle());
                combatant.setCurrentSpell(combatant.getSpells().get(index).getName());
            }
        });
        rbgCombatantRBGroup.getToggles().get(0).setSelected(true);
        return vbCombatantSpells;
    }

    public void setAISpell(Hero combatant){
        boolean spellSellected = false;
        do{
            Random random = new Random();
            int spellIndex = random.nextInt(combatant.getSpells().size());
            Spell spell = combatant.getSpells().get(spellIndex);
            if((spell.getAmountOfPowerResourceRequiredToCast() <= combatant.getCurrentPowerResource()) &&
                    spell.getCurrentCooldown() == spell.getCooldown()){
                combatant.setCurrentSpell(spell.getName());
                spellSellected = true;
            }
        } while (!spellSellected);
    }

    public Hero selectOpponent(ArrayList<Hero> opponents){
        Hero opponent;
        do{
            Random random = new Random();
            int opponentIndex = random.nextInt(opponents.size());
            opponent = opponents.get(opponentIndex);
        } while(opponent.getCurrentHealth() <=0);
        return opponent;
    }

    public void calculateExperienceAndSaveHero(Hero hero, String victoryCondition){
        switch (victoryCondition){
            case "victory":{
                hero.setCurrentExperience(hero. getCurrentExperience() + 1000);
                break;
            }
            case "draw":{
                hero.setCurrentExperience(hero. getCurrentExperience() + 500);
                break;
            }
        }
        if(hero.getCurrentExperience() >= hero.getMaxExperience()){
            hero.setCurrentExperience(0);
            hero.setCurrentLevel(hero.getCurrentLevel() + 1);
        }
        if(hero.getCurrentLevel() >= hero.getMaxLevel()){
            hero.setCurrentExperience(hero.getMaxExperience());
            hero.setCurrentLevel(hero.getMaxLevel());
        }
        connectionService.updateHero(hero);
    }

}
