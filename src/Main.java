import scenes.LoginScene;
import services.ConnectionServiceSqliteImpl;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage){
        //Load login scene as first scene in app
        new LoginScene(stage, new ConnectionServiceSqliteImpl());
    }
}
