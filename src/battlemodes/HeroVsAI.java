package battlemodes;

import javafx.scene.text.Text;
import scenes.SetupScene;
import scenes.StatisticsScene;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import models.arenas.Arena;
import models.buffsdebuffs.BuffDebuff;
import models.buffsdebuffs.RegeneratePowerResource;
import models.heroes.Hero;
import services.*;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HeroVsAI extends BattleMode{

    private LoggingService loggingService;
    private Logger logger;
    private ConnectionService connectionService;
    private Stage stage;
    private GUIService guiService;
    private BattleModeService battleModeService;
    private Hero hero;
    private Hero opponent;
    private ArrayList<Hero> allOpponents = new ArrayList<>();
    private VBox vbHeroStats;
    private VBox vbOpponentStats;
    private VBox vbHeroSpells;
    private VBox vbOpponentSpells;
    private Label statusLine;
    private TextArea taLog = new TextArea();
    private Button btnStatistics = new Button("Statistics");
    private Button btnAttack = new Button("Attack!");
    private int heroStatisticsHealth = 0;
    private int opponentStatisticsHealth = 0;
    private int heroStatisticsPower = 0;
    private int opponentStatisticsPower = 0;

    public HeroVsAI(LoggingService loggingService, ConnectionService connectionService, GUIService guiService, BattleModeService battleModeService, Hero hero, Arena arena, Stage stage){
        this.loggingService = loggingService;
        this.logger = loggingService.getLogger();
        this.connectionService = connectionService;
        this.hero = hero;
        this.opponent = battleModeService.createNewHero("random");
        this.arena = arena;
        this.stage = stage;
        this.guiService = guiService;
        this.battleModeService = battleModeService;
        createArena();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Hero getOpponent() {
        return opponent;
    }

    public void setOpponent(Hero opponent) {
        this.opponent = opponent;
    }

    private void createArena(){
        //Create main grid pane
        GridPane gpMain = new GridPane();
        gpMain.setAlignment(Pos.CENTER);
        gpMain.setId("gpMain");

        //Arena info
        VBox vbArena = new VBox();
        vbArena.setAlignment(Pos.CENTER);
        Text arenaName = new Text();
        arenaName.setText(arena.getName());
        arenaName.setId("arenaName");
        vbArena.getChildren().add(arenaName);
        Label arenaDescription = new Label();
        arenaDescription.setWrapText(true);
        arenaDescription.setText(arena.getArenaDescription());
        vbArena.getChildren().add(arenaDescription);
        Label arenaBuffsDebuffs = new Label("Buffs/debuffs: ");
        arenaBuffsDebuffs.setWrapText(true);
        for(BuffDebuff buffDebuff : arena.getBuffDebuffs()){
            arenaBuffsDebuffs.setText(arenaBuffsDebuffs.getText() + "*" + buffDebuff.getName() + "*");
        }
        vbArena.getChildren().add(arenaBuffsDebuffs);
        gpMain.add(vbArena,0 ,0, GridPane.REMAINING, 1);

        //Right heroes gridpane
        GridPane gpRightHeroes = new GridPane();
        gpRightHeroes.getStyleClass().add("gpHeroes");

        //Hero Vbox
        VBox vbHero = new VBox();

        //Hero info
        GridPane gpHero = new GridPane();
        gpHero.getStyleClass().add("gpHeroesLabels");
        VBox vbHeroLabels = new VBox();
        vbHeroLabels = guiService.createBasicHeroLabels(vbHeroLabels);
        gpHero.add(vbHeroLabels, 0, 0);
        vbHeroStats = new VBox();
        vbHeroStats = guiService.createBasicHeroLabels(vbHeroStats);
        gpHero.add(vbHeroStats, 1, 0);
        vbHero.getChildren().add(gpHero);
        vbHeroStats = guiService.displayBasicHeroStats(vbHeroStats, hero);

        //Create hero spells
        vbHeroSpells = new VBox();
        vbHeroSpells = battleModeService.setCombatantSpells(vbHeroSpells, hero);
        vbHero.getChildren().add(vbHeroSpells);

        gpRightHeroes.add(vbHero, 0, 0);

        gpMain.add(gpRightHeroes, 2, 1);

        //Left heroes gridpane
        GridPane gpLeftHeroes = new GridPane();
        gpLeftHeroes.getStyleClass().add("gpHeroes");

        //Opponent Vbox
        VBox vbOpponent = new VBox();

        //Opponent info
        GridPane gpOpponent = new GridPane();
        gpOpponent.getStyleClass().add("gpHeroesLabels");
        VBox vbOpponentLabels = new VBox();
        vbOpponentLabels = guiService.createBasicHeroLabels(vbOpponentLabels);
        gpOpponent.add(vbOpponentLabels, 0, 0);
        vbOpponentStats = new VBox();
        vbOpponentStats = guiService.createBasicHeroLabels(vbOpponentStats);
        gpOpponent.add(vbOpponentStats, 1, 0);
        vbOpponent.getChildren().add(gpOpponent);
        vbOpponentStats = guiService.displayBasicHeroStats(vbOpponentStats, opponent);

        //Create opponent spells
        vbOpponentSpells = new VBox();
        vbOpponentSpells = battleModeService.setCombatantSpells(vbOpponentSpells, opponent);
        vbOpponentSpells.setDisable(true);
        vbOpponent.getChildren().add(vbOpponentSpells);

        gpLeftHeroes.add(vbOpponent, 0, 0);

        gpMain.add(gpLeftHeroes, 0, 1);

        //Adds buttons
        Button btnQuit = new Button("Quit");
        btnQuit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new SetupScene(stage, connectionService, guiService);
            }
        });
        gpMain.add(btnQuit, 0, 3);
        btnStatistics.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Map<String, Integer> healthStatistics = new HashMap<>();
                Map<String, Integer> powerStatistics = new HashMap<>();
                healthStatistics.put(hero.getHeroName(), heroStatisticsHealth);
                healthStatistics.put(opponent.getHeroName(), opponentStatisticsHealth);
                powerStatistics.put(hero.getHeroName(), heroStatisticsPower);
                powerStatistics.put(opponent.getHeroName(), opponentStatisticsPower);
                new StatisticsScene(connectionService, guiService, healthStatistics, powerStatistics, stage);
            }
        });
        btnStatistics.setDisable(true);
        gpMain.add(btnStatistics, 1, 3);
        btnAttack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resolveTurn();
            }
        });
        gpMain.add(btnAttack, 2, 3);

        //Create textarea for logging
        taLog = new TextArea();
        taLog.setWrapText(true);
        taLog.setEditable(false);
        ScrollPane spLog = new ScrollPane(taLog);
        spLog.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        TextAreaAppender.textArea = taLog;
        gpMain.add(taLog, 1, 1);

        //Setup logger header
        logger.info("ARENA: " + arena.getName());
        logger.info("BATTLE MODE: Hero vs AI");
        logger.info("COMBATANTS: " + hero.getHeroName() + " and " + opponent.getHeroName());

        //Create status line
        statusLine = new Label("Fight!");
        gpMain.add(statusLine, 0, 4);

        //Set scene and show
        Scene scene = new Scene(gpMain, 1200, 700);
        scene.getStylesheets().addAll("style/General.css", "style/Arena.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    private void resolveTurn(){
        if(battleModeService.getCombatantSpell(hero).getCurrentCooldown() < battleModeService.getCombatantSpell(hero).getCooldown()){
            statusLine.setText("Unable to attack, spell is on cooldown.");
            return;
        }
        if(hero.getCurrentPowerResource() < battleModeService.getCombatantSpell(hero).getAmountOfPowerResourceRequiredToCast()){
            statusLine.setText("You dont have enough power for that.");
            return;
        }

        //Setts this values for later logging
        hero.setCurrentHealthThisTurn(hero.getCurrentHealth());
        opponent.setCurrentHealthThisTurn(opponent.getCurrentHealth());
        hero.setCurrentPowerResourceThisTurn(hero.getCurrentPowerResource());
        opponent.setCurrentPowerResourceThisTurn(opponent.getCurrentPowerResource());

        taLog.setText("");
        logger.info("NEW TURN");

        statusLine.setText("Fight!");
        battleModeService.resolveAllSpellCooldowns(hero);
        battleModeService.resolveAllSpellCooldowns(opponent);

        //Add power regeneration buff
        battleModeService.resolveArenaAndBattleModeBuffsDebuffs(arena, new RegeneratePowerResource(), hero);
        battleModeService.resolveArenaAndBattleModeBuffsDebuffs(arena, new RegeneratePowerResource(), opponent);

        //Select opponent and add buffsdebuffs
        allOpponents.clear();
        allOpponents.add(opponent);
        Hero sellectedOpponent =  battleModeService.selectOpponent(allOpponents);
        battleModeService.addCombatantSpellBuffsDebuffs(hero, sellectedOpponent);

        battleModeService.setAISpell(opponent);
        allOpponents.clear();
        allOpponents.add(hero);
        sellectedOpponent = battleModeService.selectOpponent(allOpponents);
        battleModeService.addCombatantSpellBuffsDebuffs(opponent, sellectedOpponent);

        battleModeService.resolveCombatantBuffsDebuffs(hero);
        battleModeService.resolveCombatantBuffsDebuffs(opponent);

        battleModeService.removeFinishedBuffsDebuffs(hero);
        battleModeService.removeFinishedBuffsDebuffs(opponent);

        battleModeService.fixCurrentHealthOverflow(hero);
        battleModeService.fixCurrentHealthOverflow(opponent);
        battleModeService.fixCurrentPowerResourceOverflow(hero);
        battleModeService.fixCurrentPowerResourceOverflow(opponent);

        guiService.displayBasicHeroStats(vbHeroStats, hero);
        guiService.displayBasicHeroStats(vbOpponentStats, opponent);
        battleModeService.refreshSpellsText(vbHeroSpells, hero);
        battleModeService.refreshSpellsText(vbOpponentSpells, opponent);

        logger.info("");
        logger.info(loggingService.getTurnInfo(hero.getCurrentHealthThisTurn(), hero.getCurrentPowerResourceThisTurn(), hero));
        logger.info(loggingService.getTurnInfo(opponent.getCurrentHealthThisTurn(), opponent.getCurrentPowerResourceThisTurn(), opponent));

        //Update statistics
        heroStatisticsHealth += Math.abs(hero.getCurrentHealth() - hero.getCurrentHealthThisTurn());
        opponentStatisticsHealth += Math.abs(opponent.getCurrentHealth() - opponent.getCurrentHealthThisTurn());
        heroStatisticsPower += Math.abs(hero.getCurrentPowerResource() - hero.getCurrentPowerResourceThisTurn());
        opponentStatisticsPower += Math.abs(opponent.getCurrentPowerResource() - opponent.getCurrentPowerResourceThisTurn());

        checkVictoryCondition();
    }

    private void checkVictoryCondition(){
        //Check if defeat
        if(hero.getCurrentHealth() <= 0){
            statusLine.setText("Defeat!");
            btnStatistics.setDisable(false);
            btnAttack.setDisable(true);
        }
        //Check if draw
        if((hero.getCurrentHealth() <= 0) && (opponent.getCurrentHealth() <=0)){
            statusLine.setText("Draw!");
            btnStatistics.setDisable(false);
            btnAttack.setDisable(true);
            battleModeService.calculateExperienceAndSaveHero(hero, "draw");
        }
        //Check if victory
        if((opponent.getCurrentHealth() <= 0) && (hero.getCurrentHealth() > 0)){
            statusLine.setText("Victory!");
            btnStatistics.setDisable(false);
            btnAttack.setDisable(true);
            battleModeService.calculateExperienceAndSaveHero(hero, "victory");
        }

    }
}
