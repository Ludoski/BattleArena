package battlemodes;

import models.arenas.Arena;

public class BattleMode {

    Arena arena;

    public Arena getArena() {
        return arena;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }
}
