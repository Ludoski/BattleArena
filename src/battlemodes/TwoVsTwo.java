package battlemodes;

import javafx.scene.text.Text;
import scenes.SetupScene;
import scenes.StatisticsScene;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import models.arenas.Arena;
import models.buffsdebuffs.BuffDebuff;
import models.buffsdebuffs.RegeneratePowerResource;
import models.heroes.Hero;
import services.*;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TwoVsTwo extends BattleMode{

    private LoggingService loggingService;
    private Logger logger;
    private ConnectionService connectionService;
    private Stage stage;
    private GUIService guiService;
    private BattleModeService battleModeService;
    private Hero hero;
    private Hero npc;
    private Hero opponent1;
    private Hero opponent2;
    private ArrayList<Hero> allOpponents = new ArrayList<>();
    private VBox vbHeroStats;
    private VBox vbNpcStats;
    private VBox vbOpponent1Stats;
    private VBox vbOpponent2Stats;
    private VBox vbHeroSpells;
    private VBox vbNpcSpells;
    private VBox vbOpponent1Spells;
    private VBox vbOpponent2Spells;
    private Label statusLine;
    TextArea taLog = new TextArea();
    Button btnStatistics = new Button("Statistics");
    Button btnAttack = new Button("Attack!");
    int heroStatisticsHealth = 0;
    int npcStatisticsHealth = 0;
    int opponent1StatisticsHealth = 0;
    int opponent2StatisticsHealth = 0;
    int heroStatisticsPower = 0;
    int npcStatisticsPower = 0;
    int opponent1StatisticsPower = 0;
    int opponent2StatisticsPower = 0;

    public TwoVsTwo(LoggingService loggingService, ConnectionService connectionService, GUIService guiService, BattleModeService battleModeService, Hero hero, Arena arena, Stage stage){
        this.loggingService = loggingService;
        this.logger = loggingService.getLogger();
        this.connectionService = connectionService;
        this.hero = hero;
        this.npc = battleModeService.createNewHero("random");
        this.opponent1 = battleModeService.createNewHero("random");
        this.opponent2 = battleModeService.createNewHero("random");
        this.arena = arena;
        this.stage = stage;
        this.guiService = guiService;
        this.battleModeService = battleModeService;
        createArena();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Hero getNpc() {
        return npc;
    }

    public void setNpc(Hero npc) {
        this.npc = npc;
    }

    public Hero getOpponent1() {
        return opponent1;
    }

    public void setOpponent1(Hero opponent1) {
        this.opponent1 = opponent1;
    }

    public Hero getOpponent2() {
        return opponent2;
    }

    public void setOpponent2(Hero opponent2) {
        this.opponent2 = opponent2;
    }

    private void createArena(){
        //Create main grid pane
        GridPane gpMain = new GridPane();
        gpMain.setAlignment(Pos.CENTER);
        gpMain.setId("gpMain");

        //Arena info
        VBox vbArena = new VBox();
        vbArena.setAlignment(Pos.CENTER);
        Text arenaName = new Text();
        arenaName.setText(arena.getName());
        arenaName.setId("arenaName");
        vbArena.getChildren().add(arenaName);
        Label arenaDescription = new Label();
        arenaDescription.setWrapText(true);
        arenaDescription.setText(arena.getArenaDescription());
        vbArena.getChildren().add(arenaDescription);
        Label arenaBuffsDebuffs = new Label("Buffs/debuffs: ");
        arenaBuffsDebuffs.setWrapText(true);
        for(BuffDebuff buffDebuff : arena.getBuffDebuffs()){
            arenaBuffsDebuffs.setText(arenaBuffsDebuffs.getText() + "*" + buffDebuff.getName() + "*");
        }
        vbArena.getChildren().add(arenaBuffsDebuffs);
        gpMain.add(vbArena,0 ,0, GridPane.REMAINING, 1);

        //Right heroes gridpane
        GridPane gpRightHeroes = new GridPane();
        gpRightHeroes.getStyleClass().add("gpHeroes");

        //Hero Vbox
        VBox vbHero = new VBox();

        //Hero info
        GridPane gpHero = new GridPane();
        gpHero.getStyleClass().add("gpHeroesLabels");
        VBox vbHeroLabels = new VBox();
        vbHeroLabels = guiService.createBasicHeroLabels(vbHeroLabels);
        gpHero.add(vbHeroLabels, 0, 0);
        vbHeroStats = new VBox();
        vbHeroStats = guiService.createBasicHeroLabels(vbHeroStats);
        gpHero.add(vbHeroStats, 1, 0);
        vbHero.getChildren().add(gpHero);
        vbHeroStats = guiService.displayBasicHeroStats(vbHeroStats, hero);

        //Create hero spells
        vbHeroSpells = new VBox();
        vbHeroSpells = battleModeService.setCombatantSpells(vbHeroSpells, hero);
        vbHero.getChildren().add(vbHeroSpells);

        gpRightHeroes.add(vbHero, 0, 2);

        //Separator
        Separator heroAndNpcSeparator = new Separator();
        gpRightHeroes.add(heroAndNpcSeparator, 0, 1);

        //Hero Vbox
        VBox vbNpc = new VBox();

        //Npc info
        GridPane gpNpc = new GridPane();
        gpNpc.getStyleClass().add("gpHeroesLabels");
        VBox vbNpcLabels = new VBox();
        vbNpcLabels = guiService.createBasicHeroLabels(vbNpcLabels);
        gpNpc.add(vbNpcLabels, 0, 0);
        vbNpcStats = new VBox();
        vbNpcStats = guiService.createBasicHeroLabels(vbNpcStats);
        gpNpc.add(vbNpcStats, 1, 0);
        vbNpc.getChildren().add(gpNpc);
        vbNpcStats = guiService.displayBasicHeroStats(vbNpcStats, npc);

        //Create npc spells
        vbNpcSpells = new VBox();
        vbNpcSpells = battleModeService.setCombatantSpells(vbNpcSpells, npc);
        vbNpcSpells.setDisable(true);
        vbNpc.getChildren().add(vbNpcSpells);

        gpRightHeroes.add(vbNpc, 0, 0);

        gpMain.add(gpRightHeroes, 2, 1);

        //Left heroes gridpane
        GridPane gpLeftHeroes = new GridPane();
        gpLeftHeroes.getStyleClass().add("gpHeroes");

        //Opponent1 Vbox
        VBox vbOpponent1 = new VBox();

        //Opponent1 info
        GridPane gpOpponent1 = new GridPane();
        gpOpponent1.getStyleClass().add("gpHeroesLabels");
        VBox vbOpponent1Labels = new VBox();
        vbOpponent1Labels = guiService.createBasicHeroLabels(vbOpponent1Labels);
        gpOpponent1.add(vbOpponent1Labels, 0, 0);
        vbOpponent1Stats = new VBox();
        vbOpponent1Stats = guiService.createBasicHeroLabels(vbOpponent1Stats);
        gpOpponent1.add(vbOpponent1Stats, 1, 0);
        vbOpponent1.getChildren().add(gpOpponent1);
        vbOpponent1Stats = guiService.displayBasicHeroStats(vbOpponent1Stats, opponent1);

        //Create opponent1 spells
        vbOpponent1Spells = new VBox();
        vbOpponent1Spells = battleModeService.setCombatantSpells(vbOpponent1Spells, opponent1);
        vbOpponent1Spells.setDisable(true);
        vbOpponent1.getChildren().add(vbOpponent1Spells);

        gpLeftHeroes.add(vbOpponent1, 0, 0);

        //Separator
        Separator opponent1AndOpponent2Separator = new Separator();
        gpLeftHeroes.add(opponent1AndOpponent2Separator, 0, 1);

        //Opponent2 Vbox
        VBox vbOpponent2 = new VBox();

        //Opponent2 info
        GridPane gpOpponent2 = new GridPane();
        gpOpponent2.getStyleClass().add("gpHeroesLabels");
        VBox vbOpponent2Labels = new VBox();
        vbOpponent2Labels = guiService.createBasicHeroLabels(vbOpponent2Labels);
        gpOpponent2.add(vbOpponent2Labels, 0, 0);
        vbOpponent2Stats = new VBox();
        vbOpponent2Stats = guiService.createBasicHeroLabels(vbOpponent2Stats);
        gpOpponent2.add(vbOpponent2Stats, 1, 0);
        vbOpponent2.getChildren().add(gpOpponent2);
        vbOpponent2Stats = guiService.displayBasicHeroStats(vbOpponent2Stats, opponent2);

        //Create opponent2 spells
        vbOpponent2Spells = new VBox();
        vbOpponent2Spells = battleModeService.setCombatantSpells(vbOpponent2Spells, opponent2);
        vbOpponent2Spells.setDisable(true);
        vbOpponent2.getChildren().add(vbOpponent2Spells);

        gpLeftHeroes.add(vbOpponent2, 0, 2);

        gpMain.add(gpLeftHeroes, 0, 1);

        //Adds buttons
        Button btnQuit = new Button("Quit");
        btnQuit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new SetupScene(stage, connectionService, guiService);
            }
        });
        gpMain.add(btnQuit, 0, 3);
        btnStatistics.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Map<String, Integer> healthStatistics = new HashMap<>();
                Map<String, Integer> powerStatistics = new HashMap<>();
                healthStatistics.put(hero.getHeroName(), heroStatisticsHealth);
                healthStatistics.put(npc.getHeroName(), npcStatisticsHealth);
                healthStatistics.put(opponent1.getHeroName(), opponent1StatisticsHealth);
                healthStatistics.put(opponent2.getHeroName(), opponent2StatisticsHealth);
                powerStatistics.put(hero.getHeroName(), heroStatisticsPower);
                powerStatistics.put(npc.getHeroName(), heroStatisticsPower);
                powerStatistics.put(opponent1.getHeroName(), opponent1StatisticsPower);
                powerStatistics.put(opponent2.getHeroName(), opponent2StatisticsPower);
                new StatisticsScene(connectionService, guiService, healthStatistics, powerStatistics, stage);
            }
        });
        btnStatistics.setDisable(true);
        gpMain.add(btnStatistics, 1, 3);
        btnAttack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resolveTurn();
            }
        });
        gpMain.add(btnAttack, 2, 3);

        //Create textarea for logging
        taLog = new TextArea();
        taLog.setWrapText(true);
        taLog.setEditable(false);
        ScrollPane spLog = new ScrollPane(taLog);
        spLog.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        TextAreaAppender.textArea = taLog;
        gpMain.add(taLog, 1, 1);

        //Setup logger header
        logger.info("ARENA: " + arena.getName());
        logger.info("BATTLE MODE: Hero vs AI");
        logger.info("COMBATANTS: " + hero.getHeroName() + " and " + npc.getHeroName() + " versus "
                                    + opponent1.getHeroName() + " and " + opponent2.getHeroName());

        //Create status line
        statusLine = new Label("Fight!");
        gpMain.add(statusLine, 0, 4);

        //Set scene and show
        Scene scene = new Scene(gpMain, 1200, 700);
        scene.getStylesheets().addAll("style/General.css", "style/Arena.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    private void resolveTurn(){
        if(battleModeService.getCombatantSpell(hero).getCurrentCooldown() < battleModeService.getCombatantSpell(hero).getCooldown()){
            statusLine.setText("Unable to attack, spell is on cooldown.");
            return;
        }
        if(hero.getCurrentPowerResource() < battleModeService.getCombatantSpell(hero).getAmountOfPowerResourceRequiredToCast()){
            statusLine.setText("You dont have enough power for that.");
            return;
        }

        //Setts this values for later logging
        hero.setCurrentHealthThisTurn(hero.getCurrentHealth());
        npc.setCurrentHealthThisTurn(npc.getCurrentHealth());
        opponent1.setCurrentHealthThisTurn(opponent1.getCurrentHealth());
        opponent2.setCurrentHealthThisTurn(opponent2.getCurrentHealth());
        hero.setCurrentPowerResourceThisTurn(hero.getCurrentPowerResource());
        npc.setCurrentPowerResourceThisTurn(npc.getCurrentPowerResource());
        opponent1.setCurrentPowerResourceThisTurn(opponent1.getCurrentPowerResource());
        opponent2.setCurrentPowerResourceThisTurn(opponent2.getCurrentPowerResource());

        taLog.setText("");
        logger.info("NEW TURN");

        statusLine.setText("Fight!");
        battleModeService.resolveAllSpellCooldowns(hero);
        battleModeService.resolveAllSpellCooldowns(npc);
        battleModeService.resolveAllSpellCooldowns(opponent1);
        battleModeService.resolveAllSpellCooldowns(opponent2);

        //Add power regeneration buff
        battleModeService.resolveArenaAndBattleModeBuffsDebuffs(arena, new RegeneratePowerResource(), hero);
        battleModeService.resolveArenaAndBattleModeBuffsDebuffs(arena, new RegeneratePowerResource(), npc);
        battleModeService.resolveArenaAndBattleModeBuffsDebuffs(arena, new RegeneratePowerResource(), opponent1);
        battleModeService.resolveArenaAndBattleModeBuffsDebuffs(arena, new RegeneratePowerResource(), opponent2);

        //Select opponent and add buffsdebuffs
        allOpponents.clear();
        allOpponents.add(opponent1);
        allOpponents.add(opponent2);
        Hero sellectedOpponent =  battleModeService.selectOpponent(allOpponents);
        battleModeService.addCombatantSpellBuffsDebuffs(hero, sellectedOpponent);

        battleModeService.setAISpell(npc);
        allOpponents.clear();
        allOpponents.add(opponent1);
        allOpponents.add(opponent2);
        sellectedOpponent = battleModeService.selectOpponent(allOpponents);
        battleModeService.addCombatantSpellBuffsDebuffs(npc, sellectedOpponent);

        battleModeService.setAISpell(opponent1);
        allOpponents.clear();
        allOpponents.add(hero);
        allOpponents.add(npc);
        sellectedOpponent = battleModeService.selectOpponent(allOpponents);
        battleModeService.addCombatantSpellBuffsDebuffs(opponent1, sellectedOpponent);

        battleModeService.setAISpell(opponent2);
        allOpponents.clear();
        allOpponents.add(hero);
        allOpponents.add(npc);
        sellectedOpponent = battleModeService.selectOpponent(allOpponents);
        battleModeService.addCombatantSpellBuffsDebuffs(opponent2, sellectedOpponent);

        battleModeService.resolveCombatantBuffsDebuffs(hero);
        battleModeService.resolveCombatantBuffsDebuffs(npc);
        battleModeService.resolveCombatantBuffsDebuffs(opponent1);
        battleModeService.resolveCombatantBuffsDebuffs(opponent2);

        battleModeService.removeFinishedBuffsDebuffs(hero);
        battleModeService.removeFinishedBuffsDebuffs(npc);
        battleModeService.removeFinishedBuffsDebuffs(opponent1);
        battleModeService.removeFinishedBuffsDebuffs(opponent2);

        battleModeService.fixCurrentHealthOverflow(hero);
        battleModeService.fixCurrentHealthOverflow(npc);
        battleModeService.fixCurrentHealthOverflow(opponent1);
        battleModeService.fixCurrentHealthOverflow(opponent2);
        battleModeService.fixCurrentPowerResourceOverflow(hero);
        battleModeService.fixCurrentPowerResourceOverflow(npc);
        battleModeService.fixCurrentPowerResourceOverflow(opponent1);
        battleModeService.fixCurrentPowerResourceOverflow(opponent2);

        guiService.displayBasicHeroStats(vbHeroStats, hero);
        guiService.displayBasicHeroStats(vbNpcStats, npc);
        guiService.displayBasicHeroStats(vbOpponent1Stats, opponent1);
        guiService.displayBasicHeroStats(vbOpponent2Stats, opponent2);
        battleModeService.refreshSpellsText(vbHeroSpells, hero);
        battleModeService.refreshSpellsText(vbNpcSpells, npc);
        battleModeService.refreshSpellsText(vbOpponent1Spells, opponent1);
        battleModeService.refreshSpellsText(vbOpponent2Spells, opponent2);

        logger.info("");
        logger.info(loggingService.getTurnInfo(hero.getCurrentHealthThisTurn(), hero.getCurrentPowerResourceThisTurn(), hero));
        logger.info(loggingService.getTurnInfo(npc.getCurrentHealthThisTurn(), npc.getCurrentPowerResourceThisTurn(), npc));
        logger.info(loggingService.getTurnInfo(opponent1.getCurrentHealthThisTurn(), opponent1.getCurrentPowerResourceThisTurn(), opponent1));
        logger.info(loggingService.getTurnInfo(opponent2.getCurrentHealthThisTurn(), opponent2.getCurrentPowerResourceThisTurn(), opponent2));

        //Update statistics
        heroStatisticsHealth += Math.abs(hero.getCurrentHealth() - hero.getCurrentHealthThisTurn());
        npcStatisticsHealth += Math.abs(npc.getCurrentHealth() - npc.getCurrentPowerResourceThisTurn());
        opponent1StatisticsHealth += Math.abs(opponent1.getCurrentHealth() - opponent1.getCurrentHealthThisTurn());
        opponent2StatisticsHealth += Math.abs(opponent2.getCurrentHealth() - opponent2.getCurrentHealthThisTurn());
        heroStatisticsPower += Math.abs(hero.getCurrentPowerResource() - hero.getCurrentPowerResourceThisTurn());
        npcStatisticsPower += Math.abs(npc.getCurrentPowerResource() - npc.getCurrentPowerResourceThisTurn());
        opponent1StatisticsPower += Math.abs(opponent1.getCurrentPowerResource() - opponent1.getCurrentPowerResourceThisTurn());
        opponent2StatisticsPower += Math.abs(opponent2.getCurrentPowerResource() - opponent2.getCurrentPowerResourceThisTurn());

        checkVictoryCondition();
    }

    private void checkVictoryCondition(){
        //Check if defeat
        if(hero.getCurrentHealth() <= 0){
            statusLine.setText("Defeat!");
            btnStatistics.setDisable(false);
            btnAttack.setDisable(true);
        }
        //Check if draw
        if((hero.getCurrentHealth() <= 0) && (npc.getCurrentHealth() <=0) &&
                (opponent1.getCurrentHealth() <= 0) && (opponent2.getCurrentHealth() <= 0)){
            statusLine.setText("Draw!");
            btnStatistics.setDisable(false);
            btnAttack.setDisable(true);
            battleModeService.calculateExperienceAndSaveHero(hero, "draw");
        }
        //Check if victory
        if(((opponent1.getCurrentHealth() <= 0) && (opponent2.getCurrentHealth() <= 0)) &&
                ((hero.getCurrentHealth() > 0) || (npc.getCurrentHealth() > 0))){
            statusLine.setText("Victory!");
            btnStatistics.setDisable(false);
            btnAttack.setDisable(true);
            battleModeService.calculateExperienceAndSaveHero(hero, "victory");
        }

    }
}
