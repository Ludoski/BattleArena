BATTLE ARENA

Battle Arena is a game in which player chooses a hero and battles other heroes, alone or with some help from NPC characters.

Login screen:
At this screen you can create your account or login if you already have an account.
Enter username and password and click on login or register button.

Setup screen:
Here you can setup your next battle and check your heroes.
SELECT ARENA - Choose arena, different arenas differently affects the gameplay.
SELECT HERO - Choose from one of your heroes. When you choose hero you can see all his stats.
SELECT BATTLE MODE - Choose your battle mode (Hero vs. AI, Hero and NPC vs. 2 AIs,...).
You can also rename your hero, delete hero or delete your account.

Battle arena:
This is your battlefield. Select one of your spells and attack. Spells cost power resource so think ahead. Basic attack cost nothing and you can use it every turn.
Turn results are displayed in the middle. After the end of the battle you can quit and return to setup screen or view statistics.

Statistics screen:
Here you can check how you and your alies and opponents performed.